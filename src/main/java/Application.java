import java.io.*;
import java.util.*;

/**
 * Задача
 * Разработать программу для обработки файла, в котором задаются зависимости сущностей друг от друга. Каждая сущность
 * представлена своим уникальным идентификатором в виде целого числа. Зависимости задаются строкой в текстовом файле,
 * содержащим произвольное количество строк формата: <id сущности> <id сущности, от которой она зависит>. Файл читать
 * из STDIN. Для прочитанных сведений о связях между сущностями необходимо найти циклические зависимости и вывести их
 * в STDOUT в виде id сущностей через пробел.
 *
 * Created by Olga Svechnikova on 22.11.15.
 */
public class Application {
    public static void main(String[] args) {
        String fileName = args[0];
        Map<Integer, Integer> pairs = new LinkedHashMap<>();
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                if (isValid(line)) {
                    String[] strArr = line.split("\\s");
                    Integer id = Integer.valueOf(strArr[0]);
                    Integer parentId = Integer.valueOf(strArr[1]);
                    pairs.put(id, parentId);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        List<String> cyclicDependencies = new ArrayList<>();
        if (!pairs.isEmpty()) {
            Iterator iterator = pairs.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>) iterator.next();
                if (pairs.containsKey(pair.getValue()) && pairs.get(pair.getValue()).equals(pair.getKey())) {
                    cyclicDependencies.add(String.format("%d %d %d", pair.getKey(), pair.getValue(), pair.getKey()));
                    iterator.remove();
                }
            }
        }
        if (cyclicDependencies.size() != 0) {
            for (String row : cyclicDependencies) {
                System.out.println(row);
            }
        }
    }

    private static boolean isValid(String line) {
        String[] array = line.split("\\s");

        if (array.length == 2 && isInteger(array[0]) && isInteger(array[1])) return true;
        return false;
    }

    private static boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}